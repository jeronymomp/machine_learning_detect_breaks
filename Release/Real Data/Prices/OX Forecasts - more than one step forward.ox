#include <oxstd.oxh>
#import <packages/PcGive/pcgive_ects>
#include <oxdraw.oxh>
#include <oxprob.oxh>
#import <modelbase>
#import <simulator>
#include <packages/x12ox/x12ox.oxh>
#import <packages/PcGive/pcgive_switching>

//This code implements the automatic part of Autometrics base model estimation as Robust Method Forecast

//This method implements all forecasting methods - have to change folder name

ForecastFunction(const InitialSample, const FinalSample, const ForecastStep, const LagTestWindow, const ForecastHorizon, const Variable)

{

//Get Data to be forecast
	decl ForData = loadmat("C:\\Dados\\Git Pessoal\\cluster-analysis-for-model-selection\\Release\\2.0.0\\Real Data\\Prices\\EU Prices.xlsx");

//Get principal variable to matrix
	decl Principal = new Database();
	Principal.Load("C:\\Dados\\Git Pessoal\\cluster-analysis-for-model-selection\\Release\\2.0.0\\Real Data\\Prices\\EU Prices.xlsx");

	decl Var = Principal.GetVar(Variable);

//Global Variables
	decl h = ForecastHorizon;
	decl ForecMatrix = new matrix[rows(ForData)][9];

//Start for loop with forecast over rows
	for(h=0; h<ForecastHorizon; h++)
	{
//Start seed
				ranseed(0);

				//1 - AUTOMETRICS - ROBUST AND REGULAR VERSION	- outlier = "none"
				
//1.0 - Declare Autometrics - robust and regular models
				decl AutoModel = new PcGive();
							
//1.1 - Create Model with Autometrics
 				AutoModel.Load("C:\\Dados\\Git Pessoal\\cluster-analysis-for-model-selection\\Release\\2.0.0\\Real Data\\Prices\\EU Prices.xlsx");
				AutoModel.Deterministic(-1);
				AutoModel.DeSelect();
				AutoModel.Select("Y", {Variable, 0, LagTestWindow});
				AutoModel.Select("X", {"Constant", 0, 0});
				AutoModel.Autometrics(0.01, "none", 1);

//1.2 - Model Settings and estimation
				AutoModel.SetSelSample(InitialSample, 1, FinalSample + h, 1);
				AutoModel.SetMethod("OLS");
				AutoModel.Estimate();

//1.3 - Model Forecast
				AutoModel.TestSummary();
				decl RobustForec = AutoModel.Forecast(ForecastStep, 0, 1, 1, 1);
				decl Forec = AutoModel.Forecast(ForecastStep);

//1.4 - Send results to matrix
				ForecMatrix[FinalSample+h+ForecastStep-1][2] = RobustForec[0][1];
				ForecMatrix[FinalSample+h+ForecastStep-1][1] = Forec[0][1];
				ForecMatrix[][0] = Var;

				//2 - AUTOMETRICS - ROBUST AND REGULAR VERSION	- outlier = "IIS"
				
//2.0 - Declare Autometrics - robust and regular models
				decl AutoModel_IIS = new PcGive();
							
//2.1 - Create Model with Autometrics
 				AutoModel_IIS.Load("C:\\Dados\\Git Pessoal\\cluster-analysis-for-model-selection\\Release\\2.0.0\\Real Data\\Prices\\EU Prices.xlsx");
				AutoModel_IIS.Deterministic(-1);
				AutoModel_IIS.DeSelect();
				AutoModel_IIS.Select("Y", {Variable, 0, LagTestWindow});
				AutoModel_IIS.Select("X", {"Constant", 0, 0});
				AutoModel_IIS.Autometrics(0.01, "IIS", 1);

//2.2 - Model Settings and estimation
				AutoModel_IIS.SetSelSample(InitialSample, 1, FinalSample + h, 1);
				AutoModel_IIS.SetMethod("OLS");
				AutoModel_IIS.Estimate();

//2.3 - Model Forecast
				decl RobustForec_IIS = AutoModel_IIS.Forecast(ForecastStep, 0, 1, 1, 1);
				decl Forec_IIS = AutoModel_IIS.Forecast(ForecastStep);

//2.4 - Send results to matrix
				ForecMatrix[FinalSample+h+ForecastStep-1][5] = RobustForec_IIS[0][1];
				ForecMatrix[FinalSample+h+ForecastStep-1][6] = Forec_IIS[0][1];

	}

//Get forecast Error
ForecMatrix[][3] = ForecMatrix[][0] - ForecMatrix[][1];
ForecMatrix[][4] = ForecMatrix[][0] - ForecMatrix[][2];
ForecMatrix[][7] = ForecMatrix[][0] - ForecMatrix[][5];
ForecMatrix[][8] = ForecMatrix[][0] - ForecMatrix[][6];

				
//Create object with results
	decl DataForecast = new Database();
	decl Names={"OriginalData","AutometricsForecast","RobustForecast","AutometricsForecast_Error","RobustForecast_Error","IIS","Robust_IIS","IIS_Error","Robust_IIS_Error"};
	DataForecast.Renew(ForecMatrix,Names);
	DataForecast.SaveIn7(sprint("C:\\Dados\\Git Pessoal\\cluster-analysis-for-model-selection\\Release\\2.0.0\\Real Data\\Prices\\Ox Results\\",ForecastStep," Step\\DataForecast_",Variable,".in7"));
	DataForecast.SaveCsv(sprint("C:\\Dados\\Git Pessoal\\cluster-analysis-for-model-selection\\Release\\2.0.0\\Real Data\\Prices\\Ox Results\\",ForecastStep," Step\\DataForecast_",Variable,".csv"));
	
}

main()
{

//List of coutries to iterate over
	        decl VariableList = {"AUT","BEL","DEN","FIN","FRA","DEU","GRE","HUN","IRE","ITA","LUX","NET","NOR","POL","PRT","SPA","SWE","GBR"};
			
//Function to apply via loop - have to subtract one unit from forecast horizon (Total Sample - Training Sample) each step forward
			decl i;
			decl t;
			for(i=0;i<rows(VariableList);i++)
				{
					for(t=0;t<5;t++)	
						{
							decl var1 = 2+t;
							decl var2 = 73-t;
 							ForecastFunction(1,252,var1,50,var2,VariableList[i]);
						}
				}

}
