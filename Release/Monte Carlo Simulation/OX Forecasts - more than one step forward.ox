#include <oxstd.oxh>
#import <packages/PcGive/pcgive_ects>
#include <oxdraw.oxh>
#include <oxprob.oxh>
#import <modelbase>
#import <simulator>
#include <packages/x12ox/x12ox.oxh>
#import <packages/PcGive/pcgive_switching>

//This code implements the automatic part of Autometrics base model estimation as Robust Method Forecast

//This method implements all forecasting methods - have to change folder name

ForecastFunction(const InitialSample, const FinalSample, const ForecastStep, const LagTestWindow, const ForecastHorizon, const Variable)

{

//Get Data to be forecast
	decl ForData = loadmat("C:\\Dados\\Git Pessoal\\cluster-analysis-for-model-selection\\Release\\2.0.0\\Monte Carlo Simulation\\Dados_Artificais_diff.xlsx");

//Get principal variable to matrix
	decl Principal = new Database();
	Principal.Load("C:\\Dados\\Git Pessoal\\cluster-analysis-for-model-selection\\Release\\2.0.0\\Monte Carlo Simulation\\Dados_Artificais_diff.xlsx");

	decl Var = Principal.GetVar(Variable);

//Global Variables
	decl h = ForecastHorizon;
	decl ForecMatrix = new matrix[rows(ForData)][9];

//Start for loop with forecast over rows
	for(h=0; h<ForecastHorizon; h++)
	{
//Start seed
				ranseed(0);

				//1 - AUTOMETRICS - ROBUST AND REGULAR VERSION	- outlier = "none"
				
//1.0 - Declare Autometrics - robust and regular models
				decl AutoModel = new PcGive();
							
//1.1 - Create Model with Autometrics
 				AutoModel.Load("C:\\Dados\\Git Pessoal\\cluster-analysis-for-model-selection\\Release\\2.0.0\\Monte Carlo Simulation\\Dados_Artificais_diff.xlsx");
				AutoModel.Deterministic(-1);
				AutoModel.DeSelect();
				AutoModel.Select("Y", {Variable, 0, LagTestWindow});
				AutoModel.Select("X", {"Constant", 0, 0});
				AutoModel.Autometrics(0.01, "none", 1);

//1.2 - Model Settings and estimation
				AutoModel.SetSelSample(InitialSample, 1, FinalSample + h, 1);
				AutoModel.SetMethod("OLS");
				AutoModel.Estimate();

//1.3 - Model Forecast
				AutoModel.TestSummary();
				decl RobustForec = AutoModel.Forecast(ForecastStep, 0, 1, 1, 1);
				decl Forec = AutoModel.Forecast(ForecastStep);

//1.4 - Send results to matrix
				ForecMatrix[FinalSample+h+ForecastStep-1][2] = RobustForec[0][1];
				ForecMatrix[FinalSample+h+ForecastStep-1][1] = Forec[0][1];
				ForecMatrix[][0] = Var;

				//2 - AUTOMETRICS - ROBUST AND REGULAR VERSION	- outlier = "IIS"
				
//2.0 - Declare Autometrics - robust and regular models
				decl AutoModel_IIS = new PcGive();
							
//2.1 - Create Model with Autometrics
 				AutoModel_IIS.Load("C:\\Dados\\Git Pessoal\\cluster-analysis-for-model-selection\\Release\\2.0.0\\Monte Carlo Simulation\\Dados_Artificais_diff.xlsx");
				AutoModel_IIS.Deterministic(-1);
				AutoModel_IIS.DeSelect();
				AutoModel_IIS.Select("Y", {Variable, 0, LagTestWindow});
				AutoModel_IIS.Select("X", {"Constant", 0, 0});
				AutoModel_IIS.Autometrics(0.01, "IIS", 1);

//2.2 - Model Settings and estimation
				AutoModel_IIS.SetSelSample(InitialSample, 1, FinalSample + h, 1);
				AutoModel_IIS.SetMethod("OLS");
				AutoModel_IIS.Estimate();

//2.3 - Model Forecast
				decl RobustForec_IIS = AutoModel_IIS.Forecast(ForecastStep, 0, 1, 1, 1);
				decl Forec_IIS = AutoModel_IIS.Forecast(ForecastStep);

//2.4 - Send results to matrix
				ForecMatrix[FinalSample+h+ForecastStep-1][5] = RobustForec_IIS[0][1];
				ForecMatrix[FinalSample+h+ForecastStep-1][6] = Forec_IIS[0][1];

	}

//Get forecast Error
ForecMatrix[][3] = ForecMatrix[][0] - ForecMatrix[][1];
ForecMatrix[][4] = ForecMatrix[][0] - ForecMatrix[][2];
ForecMatrix[][7] = ForecMatrix[][0] - ForecMatrix[][5];
ForecMatrix[][8] = ForecMatrix[][0] - ForecMatrix[][6];

				
//Create object with results
	decl DataForecast = new Database();
	decl Names={"OriginalData","AutometricsForecast","RobustForecast","AutometricsForecast_Error","RobustForecast_Error","IIS","Robust_IIS","IIS_Error","Robust_IIS_Error"};
	DataForecast.Renew(ForecMatrix,Names);
	DataForecast.SaveIn7(sprint("C:\\Dados\\Git Pessoal\\cluster-analysis-for-model-selection\\Release\\2.0.0\\Monte Carlo Simulation\\Ox Results\\",ForecastStep," Step\\DataForecast_",Variable,".in7"));
	DataForecast.SaveCsv(sprint("C:\\Dados\\Git Pessoal\\cluster-analysis-for-model-selection\\Release\\2.0.0\\Monte Carlo Simulation\\Ox Results\\",ForecastStep," Step\\DataForecast_",Variable,".csv"));
	
}

main()
{

//List of coutries to iterate over
	        decl VariableList = {"sim_57","sim_58","sim_59","sim_60","sim_61","sim_62","sim_63","sim_64","sim_65","sim_66","sim_67","sim_68","sim_69","sim_70","sim_71","sim_72","sim_73","sim_74","sim_75","sim_76","sim_77","sim_78","sim_79","sim_80","sim_81","sim_82","sim_83","sim_84","sim_85","sim_86","sim_87","sim_88","sim_89","sim_90","sim_91","sim_92","sim_93","sim_94","sim_95","sim_96","sim_97","sim_98","sim_99","sim_100","sim_101","sim_102","sim_103","sim_104","sim_105","sim_106","sim_107","sim_108","sim_109","sim_110","sim_111","sim_112","sim_113","sim_114","sim_115","sim_116","sim_117","sim_118","sim_119","sim_120","sim_121","sim_122","sim_123","sim_124","sim_125","sim_126","sim_127","sim_128","sim_129","sim_130","sim_131","sim_132","sim_133","sim_134","sim_135","sim_136","sim_137","sim_138","sim_139","sim_140","sim_141","sim_142","sim_143","sim_144","sim_145","sim_146","sim_147","sim_148","sim_149","sim_150","sim_151","sim_152","sim_153","sim_154","sim_155","sim_156","sim_157","sim_158","sim_159","sim_160","sim_161","sim_162","sim_163","sim_164","sim_165","sim_166","sim_167","sim_168","sim_169","sim_170","sim_171","sim_172","sim_173","sim_174","sim_175","sim_176","sim_177","sim_178","sim_179","sim_180","sim_181","sim_182","sim_183","sim_184","sim_185","sim_186","sim_187","sim_188","sim_189","sim_190","sim_191","sim_192","sim_193","sim_194","sim_195","sim_196","sim_197","sim_198","sim_199","sim_200","sim_201","sim_202","sim_203","sim_204","sim_205","sim_206","sim_207","sim_208","sim_209","sim_210","sim_211","sim_212","sim_213","sim_214","sim_215","sim_216","sim_217","sim_218","sim_219","sim_220","sim_221","sim_222","sim_223","sim_224","sim_225","sim_226","sim_227","sim_228","sim_229","sim_230","sim_231","sim_232","sim_233","sim_234","sim_235","sim_236","sim_237","sim_238","sim_239","sim_240","sim_241","sim_242","sim_243","sim_244","sim_245","sim_246","sim_247","sim_248","sim_249","sim_250","sim_251","sim_252","sim_253","sim_254","sim_255","sim_256","sim_257","sim_258","sim_259","sim_260","sim_261","sim_262","sim_263","sim_264","sim_265","sim_266","sim_267","sim_268","sim_269","sim_270","sim_271","sim_272","sim_273","sim_274","sim_275","sim_276","sim_277","sim_278","sim_279","sim_280","sim_281","sim_282","sim_283","sim_284","sim_285","sim_286","sim_287","sim_288","sim_289","sim_290","sim_291","sim_292","sim_293","sim_294","sim_295","sim_296","sim_297","sim_298","sim_299","sim_300","sim_301","sim_302","sim_303","sim_304","sim_305","sim_306","sim_307","sim_308","sim_309","sim_310","sim_311","sim_312","sim_313","sim_314","sim_315","sim_316","sim_317","sim_318","sim_319","sim_320","sim_321","sim_322","sim_323","sim_324","sim_325","sim_326","sim_327","sim_328","sim_329","sim_330","sim_331","sim_332","sim_333","sim_334","sim_335","sim_336","sim_337","sim_338","sim_339","sim_340","sim_341","sim_342","sim_343","sim_344","sim_345","sim_346","sim_347","sim_348","sim_349","sim_350","sim_351","sim_352","sim_353","sim_354","sim_355","sim_356","sim_357","sim_358","sim_359","sim_360","sim_361","sim_362","sim_363","sim_364","sim_365","sim_366","sim_367","sim_368","sim_369","sim_370","sim_371","sim_372","sim_373","sim_374","sim_375","sim_376","sim_377","sim_378","sim_379","sim_380","sim_381","sim_382","sim_383","sim_384","sim_385","sim_386","sim_387","sim_388","sim_389","sim_390","sim_391","sim_392","sim_393","sim_394","sim_395","sim_396","sim_397","sim_398","sim_399","sim_400"};
			
//Function to apply via loop - have to subtract one unit from forecast horizon (Total Sample - Training Sample) each step forward
			decl i;
			decl t;
			for(i=0;i<rows(VariableList);i++)
				{
					for(t=0;t<1;t++)	
						{
							decl var1 = 6+t;
							decl var2 = 174-t;
 							ForecastFunction(1,20,var1,5,var2,VariableList[i]);
						}
				}

}
