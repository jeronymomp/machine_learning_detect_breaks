# Machine_Learning_Detect_Breaks

This projects carries all available data, code and results for the paper "Machine Learning Dynamic Switching Approach to Forecasting in the Presence of Structural Breaks".

## Getting started

This project consists in two parts:

- Monte Carlo Simulation 
- Real Data analysis

## Monte Carlo Simulation

We have three important sections, which are divided between files:

#### 1 - Oxmetrics

All the .ox files that are necessary:
- monte-carlo: which generates the simulated series;
- OX forecasts: which generates all the forecasts for the OX methods;
- OX forecasts more than one step: which generates all the forecasts with more than one step forward for the OX methods.

#### 2 - R

All the .rmd files that generates all forecasts for the methods not present in OX, for the ensemble methods and for DSB (proposed) method. The file is called "PI batch analysis.rmd". This file should be run after the OX part, because it uses the results from the later part.

#### Folders with results

All the results generated with Oxmetrics (folder "OX Results") and R (folder "R Results").

## Real Data

This part has two folders: Price and Production Index. Each one of those folders correspond to the files regarding the empirical analysis. Both folders have the same division:

#### 1 - Oxmetrics

All the .ox files that are necessary:
- monte-carlo: which generates the simulated series;
- OX forecasts: which generates all the forecasts for the OX methods;
- OX forecasts more than one step: which generates all the forecasts with more than one step forward for the OX methods.

#### 2 - R

All the .rmd files that generates all forecasts for the methods not present in OX, for the ensemble methods and for DSB (proposed) method. The file is called "PI batch analysis.rmd". This file should be run after the OX part, because it uses the results from the later part.

#### Folders with results

All the results generated with Oxmetrics (folder "OX Results") and R (folder "R Results").

